import { Meteor } from 'meteor/meteor';

Users = Meteor.users;


Meteor.users.allow({
  update: function(userID) {
    var u = Meteor.users.findOne(userID);
    return u;
  },
  remove: function(userID) {
    var u = Meteor.users.findOne(userID);
    return u;
  }
});

// Deny all client-side updates to user documents
Meteor.users.deny({
  update() { return true; }
});


function generateMembershipNumber(){
  const now = moment()
  const day = now.weekday() + 2; //originally 0-6; then 2-8
  const week = now.week() + 23; //1-52 + 23 -> 24 - 76
  const yearStr = (now.year() + 1).toString(); //2015 -> 2016
  const year = parseInt(yearStr[yearStr.length - 1]); //2016, get 6

  return '' + now.hour() + randomize('0', 1) + day + week + year + randomize('0', 2)
}


Accounts.onCreateUser((options, user) => {
  user.orgNumber = options.orgNumber
  user.profile = {name: options.name}
  user.mid = generateMembershipNumber() //member id for human being
  return user;
});


export default Users