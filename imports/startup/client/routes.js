import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import IndexPage from '/imports/ui/pages/index'
import MainPage from '/imports/ui/pages/main'
import Login from '/imports/ui/pages/login'
import Logup from '/imports/ui/pages/logup'

export default App = () => (
  <Router>
    <div>
      <Route  exact path="/" component={IndexPage}/>
      <Route  path="/order" component={MainPage}/>
      <Route  path="/login" component={Login}/>
      <Route  path="/logup" component={Logup}/>
    </div>
  </Router>
)