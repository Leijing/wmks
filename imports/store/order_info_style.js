import { ReactiveVar } from 'meteor/reactive-var' 

//note: es6 module is singleton instance which is created when module is loaded
export default orderInfoStyle = new ReactiveVar('table')