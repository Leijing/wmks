import { createContainer } from 'meteor/react-meteor-data';
import ContentHeader from '/imports/ui/components/content_header'

export default UserContainer = createContainer(props => {
    let user = Meteor.user()
    let userName = ''
    let avatarImgUrl = ''

    if (user) {
        userName = user.profile.name
        // avatarImgUrl = user.profile.headimgurl || ''
    }
    // if (user.services.hasOwnProperty('github')) {
    //     avatarImgUrl = 'https://avatars.githubusercontent.com/u/leijing7?v=3'
    // }
    
    return {
        userName: userName,
    };
}, ContentHeader);