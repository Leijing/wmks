import React from 'react';

import { Layout } from 'antd';
const { Content } = Layout;

import OrderType from '/imports/ui/components/order_type'
import OrderInfoCards from '/imports/ui/components/order_info'
import OrderInfoTable from '/imports/ui/components/order_table'

export default class OrderProcessContent extends React.Component {
    state = {
        orderInfoStyle: 'table'
    }
    handleClicks = (r) => {
        this.setState ({
            orderInfoStyle: r
        })
    }
    handlePlatformChange = (r) => {
        console.log(r);
    }
    handleOrderStatusChange = (r) => {
        console.log(r);
    }
    render(){
        return (
            <div>
                <Content style={{ margin: '0 16px' }}>
                    <OrderType 
                        switchBtnClicked = {this.handleClicks.bind(this)} 
                        platformChange = {this.handlePlatformChange}
                        orderStatusChange = {this.handleOrderStatusChange}
                    />
                </Content>
                <Content style={{ margin: '0 16px' }}>
                    {this.state.orderInfoStyle === 'table' ? <OrderInfoTable/> : <OrderInfoCards/> }
                </Content>
            </div>
        )
    }
}
  
