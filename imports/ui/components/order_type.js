import React from 'react';
import { Radio, Row, Col, Button, Icon } from 'antd';
const RadioGroup = Radio.Group;
const ButtonGroup = Button.Group;

export default class OrderType extends React.Component {
    constructor(props){
        super(props)
        this.platforms = ['all', 'meituan', 'eleme', 'baidu']
        this.orderStatus = ['all', 'toProcess', 'toDeliver', 'onTheWay','done','cancelled']
    }
    state = {
        orderInfoStyle: 'table' //table or cards
    }
    tableBtnClicked = () => {
        this.setState({
            orderInfoStyle: 'table'
        })
        this.props.switchBtnClicked('table')
    }
    cardsBtnClicked = () => {
        this.setState({
            orderInfoStyle: 'cards'
        })
        this.props.switchBtnClicked('cards')
    }
    handlePlatformChange = (e) => {
        this.props.platformChange(this.platforms[e.target.value])
    }
    handleOrderStatusChange = (e) => {
        this.props.orderStatusChange(this.orderStatus[e.target.value])
    }
    render(){
        return (
            <div>
                <div style={{paddingTop: 10, paddingBottom: 10}}/>
                <div style={{ padding: 24, background: '#fff', minHeight: 100 }}>
                    <Row type="flex">
                        <Col span={18}>
                            <RadioGroup onChange={this.handlePlatformChange} defaultValue={0}>
                                平台类型：&nbsp;&nbsp;&nbsp;&nbsp;
                                <Radio value={0}>全部</Radio>
                                <Radio value={1}>美团外卖</Radio>
                                <Radio value={2}>饿了么</Radio>
                                <Radio value={3}>百度(小度)</Radio>
                            </RadioGroup>
                            <br/>
                            <br/>
                            <RadioGroup onChange={this.handleOrderStatusChange} defaultValue={1}>
                                订单类型：&nbsp;&nbsp;&nbsp;&nbsp;
                                <Radio value={0}>全部</Radio>
                                <Radio value={1}>待接单</Radio>
                                <Radio value={2}>待配送</Radio>
                                <Radio value={3}>已配送</Radio>
                                <Radio value={4}>已完成</Radio>
                                <Radio value={5}>已取消</Radio>
                            </RadioGroup>
                        </Col>
                        <Col span={6}>
                            <ButtonGroup>
                                <Button type={this.state.orderInfoStyle === 'table' ?'primary':''} onClick={this.tableBtnClicked}>
                                    <Icon type='bars' />表格
                                </Button>
                                <Button type={this.state.orderInfoStyle === 'cards' ?'primary':''} onClick={this.cardsBtnClicked}>
                                    <Icon type='file' />展开
                                </Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}