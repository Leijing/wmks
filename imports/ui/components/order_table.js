import React from 'react';
import { Table, Button, Icon } from 'antd';

const columns = [{
  title: '订单编号',
  dataIndex: 'orderNum',
  render: text => <a href="#">{text}</a>,
}, {
  title: '下单时间',
  className: 'column-money',
  dataIndex: 'orderTime'
}, {
  title: '外卖平台',
  dataIndex: 'orderPlatform'
}, {
  title: '客户姓名',
  dataIndex: 'customerName'
}, {
  title: '客户电话',
  dataIndex: 'customerMobile'
}, {
  title: '订单金额(元）',
  dataIndex: 'orderAmount'
}, {
  title: '订单状态',
  dataIndex: 'orderStatus'
}, {
  title: '操作',
  render: () => <Button><Icon type='edit'/>查看</Button>
}];

const data = [{
  key: '1',
  orderNum: '4902404289',
  orderTime: '2017-07-05 09:08',
  orderPlatform: '美团 ',
  customerName: '王（先生）',
  customerMobile: '12334555676',
  orderAmount: '76.32',
  orderStatus: '已配送',
  edit: 'ops'
}, {
  key: '2',
  orderNum: '4902404288',
  orderTime: '2017-07-05 09:08',
  orderPlatform: '美团 ',
  customerName: '王（先生）',
  customerMobile: '12334555676',
  orderAmount: '76.32',
  orderStatus: '已配送',
  edit: 'ops'
}, {
  key: '3',
  orderNum: '4902404287',
  orderTime: '2017-07-05 09:08',
  orderPlatform: '美团 ',
  customerName: '王（先生）',
  customerMobile: '12334555676',
  orderAmount: '76.32',
  orderStatus: '已配送',
  edit: 'ops'
}];


export default OrderTable = () => {
    const windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    return (
        <div>
            <div style={{paddingTop: 10, paddingBottom: 10}}/>
            <div style={{ padding: 10, background: '#fff', minHeight: windowHeight - 270 }}>
                <Table
                    columns={columns}
                    dataSource={data}
                    bordered
                />
            </div>
        </div>
    )
}
