import React from 'react';
import { Icon } from 'antd';
import {
  BrowserRouter as Router,
  Link
} from 'react-router-dom'

const titleStyle = {
    padding: 15,
    fontSize: "2em", 
    color: "orange",
    width: "80%", 
    margin: "0 auto",
    textAlign: "center"
}
export default LogoTitle = () => (
    <Link to="/">
        <div style={titleStyle}>
            <Icon type="home"/> 外卖快手
        </div>
    </Link>    
)