import React from 'react';
import { Menu, Button, Icon, Dropdown, Row, Col } from 'antd';


export default class OrderInfo extends React.Component {
    render(){
        const windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        const menu = 
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key="1">同时发单</Menu.Item>
                <Menu.Item key="2">美团众包</Menu.Item>
                <Menu.Item key="3">达达配送</Menu.Item>
            </Menu>
        return (
            <div>
                <div style={{paddingTop: 10, paddingBottom: 10}}/>
                <div style={{ padding: 10, background: '#fff', minHeight: windowHeight - 270 }}>
                    <Row type="flex" justify="start" style={{fontSize:"1.5em"}}>
                        <Col span={1}><img src="imgs/mtlogo.svg" width="30"/></Col>
                        <Col span={2}>美团外卖</Col>
                        <Col span={1}></Col>
                        <Col span={1}>2#</Col>
                        <Col span={2}><span style={{color: "red"}}>立即送达</span></Col>
                        <Col span={15}></Col>
                        <Col span={2}>待配送</Col>
                    </Row>
                    
                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0"}}/>

                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={2}><span style={{fontWeight: "bold", fontSize: "1.2em"}}>叶（女士）</span></Col>
                        <Col span={2}>1378888888</Col>
                        <Col span={1}></Col>
                        <Col span={5}>美团第 2 次下单</Col>
                        <Col span={2}></Col>
                        <Col span={10}>德珍楼(正门) (麻布新村六巷七号803)</Col>
                        <Col span={2}><span style={{fontWeight: "bold", fontSize: "1.2em"}}>2.3km</span></Col>
                    </Row>
                    
                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0"}}/>

                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={2}><span style={{fontWeight: "bold", fontSize: "1.2em"}}>配送信息：</span></Col>
                        
                        <Col span={17}></Col>
                        <Col span={2}><Button>自己送餐</Button></Col>
                        <Col span={2}>
                            <Dropdown overlay={menu}>
                                <Button style={{ marginLeft: 8 }} type="primary">
                                    一键发单 <Icon type="down" />
                                </Button>
                            </Dropdown>
                        </Col>
                        <Col span={1}></Col>
                    </Row>

                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0"}}/>


                    <div style={{fontWeight: "bold", fontSize: "1.2em", padding: 10}}>商品信息：</div>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>油焖大虾</Col>
                        <Col span={6}>￥200.00</Col>
                        <Col span={6}> ×1</Col>
                        <Col span={6}>￥200.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>蒜蓉大虾</Col>
                        <Col span={6}>￥200.00</Col>
                        <Col span={6}> ×1</Col>
                        <Col span={6}>￥200.00</Col>
                    </Row>
                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0", width: "80%"}}/>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>餐盒费</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>￥10.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>配送费</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>￥15.00</Col>
                    </Row>

                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0", width: "80%"}}/>

                    <div style={{padding: 10}}>小计：</div>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>红包</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>-￥5.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>平台立减</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>-￥40.00</Col>
                    </Row>

                        <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0", width: "80%"}}/>

                        <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>用户实付</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>￥380.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>平台补贴</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>￥10.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>平台服务费</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>-￥20.00</Col>
                    </Row>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={6}>预计收入</Col>
                        <Col span={6}></Col>
                        <Col span={6}></Col>
                        <Col span={6}>￥370.00</Col>
                    </Row>

                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0"}}/>

                    <div style={{fontWeight: "bold", fontSize: "1.2em", padding: 10}}>退单信息：</div>
                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={2}>06-24 13:55:00</Col>
                        <Col span={2}>用户申请退款</Col>
                        <Col span={1}></Col>
                        <Col span={3}>申请理由：重复下单</Col>
                        <Col span={12} style={{color: "red"}}>请于30分钟内处理，否则自动退款</Col>
                        <Col span={2}><Button>同意</Button></Col>
                        <Col span={2}><Button>拒绝</Button></Col>
                    </Row>

                    <hr style={{border: "none", height: 1, color: "#F0F0F0", backgroundColor: "#F0F0F0"}}/>

                    <Row type="flex" justify="start" style={{padding: 10}}>
                        <Col span={4}>下单时间：06-24 13:15:00</Col>
                        <Col span={1}></Col>
                        <Col span={17}>订单编号：3333333333333333333333</Col>
                        <Col span={1}><Button>打印订单</Button></Col>
                        <Col span={1}></Col>
                    </Row>
                </div>
            </div>
        )
    }
}