import React from 'react';
import { Row, Col, Avatar, Icon, Button } from 'antd';
import {
  BrowserRouter as Router,
  Link
} from 'react-router-dom'
import { Meteor } from 'meteor/meteor';

export default class ContentHeader extends React.Component {
    logoutBtnClicked(){
        console.log("logged out")
        Meteor.logout(function(err){
            if (err) {
                console.log(err)
            } else {
                //use react router way for redirecting
                window.location.href = '/'
            }
        })
    }
    render(){
        return (
            <Row type="flex" justify="end">
                <Col span={18}></Col>
                <Col span={1}><Icon type="question-circle-o" /> 帮助</Col>
                <Col span={3}>
                    <Row type="flex" justify="end"> {/*if not use a new Row, 用户名 would be not aligned with the Avatar*/}
                        <Col span={8} style={{textAlign: "right"}}>
                            <Avatar icon="user" style={{marginTop: 15}}/>&nbsp;
                        </Col>
                        <Col span={14} style={{textAlign: "left"}}>
                            {this.props.userName || "未登录"}
                        </Col>
                    </Row>
                </Col>
                <Col span={2}>
                    <Button onClick={this.logoutBtnClicked}><Icon type='logout'/> 退出</Button>
                </Col>
            </Row>
        )
    }
}