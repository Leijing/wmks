import React from 'react';
import { Layout, Menu, Icon } from 'antd';
const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

import { Link } from 'react-router-dom'
import LogoTitle from '/imports/ui/components/logo_title'

export default class SidebarMenu extends React.Component {
    render(){
        return (
            <div>
                <LogoTitle />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <SubMenu
                    key="sub1"
                    title={<span><Icon type="shopping-cart" /><span className="nav-text">订单管理</span></span>}
                    >
                        <Menu.Item key="1"><Link to="/">订单处理</Link></Menu.Item>
                        <Menu.Item key="2">预订单</Menu.Item>
                        <Menu.Item key="3">催单</Menu.Item>
                        <Menu.Item key="4">退款单</Menu.Item>
                        <Menu.Item key="5">订单查询</Menu.Item>
                    </SubMenu>
                    <SubMenu
                    key="sub2"
                    title={<span><Icon type="bars" /><span className="nav-text">菜品管理</span></span>}
                    >

                    </SubMenu>
                    <SubMenu
                    key="sub3"
                    title={<span><Icon type="area-chart" /><span className="nav-text">经营分析</span></span>}
                    >

                    </SubMenu>
                    <SubMenu
                    key="sub4"
                    title={<span><Icon type="bank" /><span className="nav-text">财务管理</span></span>}
                    >
                    </SubMenu>
                    <SubMenu
                    key="sub5"
                    title={<span><Icon type="setting" /><span className="nav-text">营销活动</span></span>}
                    >
                    </SubMenu>
                    <SubMenu
                    key="sub6"
                    title={<span><Icon type="usergroup-add" /><span className="nav-text">顾客分析</span></span>}
                    >
                        <Menu.Item key="9">子菜单待定</Menu.Item>
                    </SubMenu>
                    <SubMenu
                    key="sub7"
                    title={<span><Icon type="shop" /><span className="nav-text">功能配置</span></span>}
                    >
                        <Menu.Item key="6"><Link to="/setting/user">用户设置</Link></Menu.Item>
                        <Menu.Item key="7"><Link to="/setting/takeout">外卖账户设置</Link></Menu.Item>
                        <Menu.Item key="8"><Link to="/setting/errand">跑腿账户设置</Link></Menu.Item>
                    </SubMenu>
                </Menu>
            </div>
        )
    }
}

