import React from 'react'
import { Form, Input, Button, Icon, Tooltip } from 'antd';
const FormItem = Form.Item;
import { Accounts } from 'meteor/accounts-base'
import { Meteor } from 'meteor/meteor';
import {
  BrowserRouter as Router,
  Link
} from 'react-router-dom'
import LogoTitle from '/imports/ui/components/logo_title'

class RegistrationForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const code = values.code
        Meteor.call('validateInviteCode', code, (err, res) => {
          if (err) {
            console.error(err)
            return
          }
          if (res) {
            const user = {
              orgNumber: values.orgNumber,
              username: values.mobile,
              name: values.name,
              password: values.password
            }
            this.createUser(user)
          } else {
            alert('邀请码不对')
          }
        })
      }
    });
  }
  createUser(user){
    Accounts.createUser(user, err => {
      if (err) {
        console.log("Cannot create user: ", err.reason)
        return
      }
      alert('用户注册完成')
    })
  }
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('两次输入的密码不匹配!');
    } else {
      callback();
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    return (
      <div style={{width: 450, margin: "0 auto", paddingTop: 100}}>
        <LogoTitle/>
        <Form onSubmit={this.handleSubmit} style={{paddingTop: 40}}>
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                商家号码&nbsp;
                <Tooltip title="你所在的餐厅号码">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
            hasFeedback
          >
            {getFieldDecorator('orgNumber', {
              rules: [{ required: true, message: '请输入商家号码!', whitespace: true }],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                名字
              </span>
            )}
            hasFeedback
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: '请输入您的名字!', whitespace: true }],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="手机号码"
          >
            {getFieldDecorator('mobile', {
              rules: [{ required: true, message: '请输入您的手机号码!' }],
            })(
              <Input style={{ width: '100%' }} />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="密码"
            hasFeedback
          >
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: '请输入您的密码!',
              }, {
                validator: this.checkConfirm,
              }],
            })(
              <Input type="password" />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="确认密码"
            hasFeedback
          >
            {getFieldDecorator('confirm', {
              rules: [{
                required: true, message: '请确认您的密码!',
              }, {
                validator: this.checkPassword,
              }],
            })(
              <Input type="password" />
            )}
          </FormItem>
          
          <FormItem
            {...formItemLayout}
            label={(
              <span>
                邀请码
              </span>
            )}
            hasFeedback
          >
            {getFieldDecorator('code', {
              rules: [{ required: true, message: '请输入您收到的邀请码', whitespace: true }],
            })(
              <Input />
            )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit" size="large" style={{width: 265}}>注册</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default WrappedRegistrationForm = Form.create()(RegistrationForm);

