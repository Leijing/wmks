import React from 'react'
import {
  BrowserRouter as Router,
  Link
} from 'react-router-dom'
import queryString from 'query-string'
import eleme from 'eleme-openapi-sdk'
import { Session } from 'meteor/session'

export default class IndexPage extends React.Component {
    state = {
        shopId: '',
        shopName: ''
    }
    componentDidMount(){
        console.log(this.props.match.params, this.props.location);
        const {code, state} = queryString.parse(location.search)

        if (code) {
            Meteor.call('authorizeEleme', code, state)
        } else {
            Meteor.call('getUserInfo', function(err, info){
                if (!err) {
                    console.log('User Info: ', info);
                    this.setState({
                        shopId: info.userName,
                        shopName: info.authorizedShops[0].name
                    })
                } else {
                    console.error('User Error: ', err)
                }
            }.bind(this))
        }
    }
    render(){
        return (
            <div style={{padding: 30}}>
                Index page. 
                <br/>
                <Link to="/login"> Click to login page</Link>
                <br/>
                <Link to="/order"> Click to order page</Link>
                <hr/>
                <div>商铺账号： {this.state.shopId}</div>
                <div>商铺名： {this.state.shopName}</div>
            </div>
        )
    }
}