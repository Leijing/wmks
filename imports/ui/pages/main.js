import React from 'react';

import { Layout } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import 'antd/dist/antd.css';
import SidebarMenu from '/imports/ui/components/sidebar_menu'
import UserContainer from '/imports/containers/user'
import OrderType from '/imports/ui/components/order_type'
import OrderInfo from '/imports/ui/components/order_info'
import OrderTable from '/imports/ui/components/order_table'
import OrderProcessContent from '/imports/ui/layouts/order_process'

const UserSettingContent = () => ( 
  <div>
    <div style={{paddingTop: 10, paddingBottom: 10}}/>
    <div style={{ padding: 24, background: '#fff', minHeight: 100 }}> 
      User Settings 
    </div>
  </div>
)

const TakeoutSettingContent = () => ( 
  <div>
    <div style={{paddingTop: 10, paddingBottom: 10}}/>
    <div style={{ padding: 24, background: '#fff', minHeight: 100 }}> 
      Takeout 
    </div>
  </div>
)

const ErrandSettingContent = () => ( 
  <div>
    <div style={{paddingTop: 10, paddingBottom: 10}}/>
    <div style={{ padding: 24, background: '#fff', minHeight: 100 }}> 
      Errands 
    </div>
  </div>
)

export default class MainPage extends React.Component {
  handleMenuClick(e) {
    message.info('Click on menu item.');
    console.log('click', e);
  }
  render() {
    return (
      <Layout>
        <Sider>
            <SidebarMenu />
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <UserContainer/>
          </Header>
          <Content>
            <Route exact path="/order" component={OrderProcessContent}/>
            <Route path="/setting/user" component={UserSettingContent}/>
            <Route path="/setting/takeout" component={TakeoutSettingContent}/>
            <Route path="/setting/errand" component={ErrandSettingContent}/>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            外卖快手 ©2017 
          </Footer>
        </Layout>
      </Layout>
    );
  }
}      
