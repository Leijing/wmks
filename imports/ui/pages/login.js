import React from 'react'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
const FormItem = Form.Item;
import {
  BrowserRouter as Router,
  Redirect,
  Link
} from 'react-router-dom'
import LogoTitle from '/imports/ui/components/logo_title'
import { Meteor } from 'meteor/meteor';
import eleme from 'eleme-openapi-sdk'
import { Session } from 'meteor/session'

class NormalLoginForm extends React.Component {
  state = {
    redirectToReferrer: false
  }
  componentDidUpdate(){
    Meteor.call('getAuthUrl',null, function(e, r){
        console.log(r, 9999);
        window.location = r
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        Meteor.loginWithPassword(values.mobile, values.password, (err) => {
            if (err) {
                if (err.reason === 'User not found') {
                    alert("该手机号码不存在")
                }
                if (err.reason === 'Incorrect password') {
                    alert("密码错误")
                }
                console.log("login failed: ", err.reason)
            } else {
                this.setState({ redirectToReferrer: true })
            }
        });
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    if (this.state.redirectToReferrer) {
      return (
        <Redirect to="/order"/>
      )
    }
    return (
      <div style={{backgroundColor: "#e3e3e3", height: windowHeight}}>
          <div style={{padding: 100}}/>
          <div style={{backgroundColor: "#e8e8e8", height: 500, width: "50%", margin: "0 auto"}}>
            <div style={{padding: 20}}/>
            <LogoTitle />
            <Form onSubmit={this.handleSubmit} style={{maxWidth: 300, paddingTop: 50, width: "50%", margin: "0 auto"}}>
                <FormItem>
                {getFieldDecorator('mobile', {
                    rules: [{ required: true, message: '请输入您的手机号码' }],
                })(
                    <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
                )}
                </FormItem>
                <FormItem>
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: '请输入您的密码' }],
                })(
                    <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
                )}
                </FormItem>
                <FormItem>
                    <a style={{float: "right"}} href="/">忘记密码？</a>
                    <Button type="primary" htmlType="submit" width="100">
                        登录
                    </Button>
                    &nbsp; 或 <Link to="/logup">注册</Link>
                </FormItem>
            </Form>
        </div>
      </div>
    );
  }
}

export default WrappedNormalLoginForm = Form.create()(NormalLoginForm);

