import { Meteor } from 'meteor/meteor';
import React from 'react'; 
import { render } from 'react-dom';
import '/imports/startup/client';

Meteor.startup(() => {
  render(<App />, document.getElementById('app'));
});