import md5 from 'md5'

const appKey = 'dada3f1850e68b49ced'
const appSecret = 'c9757a7bef75011f40c74002ffa20ec5'
const data = {
  "body": "{\"origin_shop_id\":\"shop001\"}",
  "format": "json",
  "timestamp": "1488363493",
  "app_key": appKey,
  "v": "1.0",
  "source_id": "73753"
}

function orderDada(){
  const ADD_ORDER_URL = "http://newopen.qa.imdada.cn/api/shop/detail";

  const sign = getSign()
  const dataInfo = Object.assign(data, {'signature':sign})

  console.log('dataInfo: ', dataInfo);
  HTTP.call('POST', ADD_ORDER_URL, {
    data: dataInfo,
    headers: {
      'Content-Type': 'application/json'
    }
  }, (error, result) => {
    if (!error) {
      console.log('--- RESULT:  ', result.data);
    } else { 
      console.error('--- ERROR: ', error);
    }
  });

}

function getSign(){
  const sortedKeyArr = _.keys(data).sort()
  let concatedStr = ''
  sortedKeyArr.forEach((e)=>{
    concatedStr += e + data[e]
  })
  concatedStr = appSecret + concatedStr + appSecret
  return md5(concatedStr).toUpperCase()
}

// orderDada()
