import { Accounts } from 'meteor/accounts-base'
import { Meteor } from 'meteor/meteor';
import moment from 'moment'
import randomize from 'randomatic'

import Users from '/imports/api/accounts/server/users.js'
import '/imports/api/eleme/server/index.js'

Meteor.startup(function() {
  console.log("Total users: ", Users.find().count());
  console.log('Server started at: ', moment().toString());
  console.log('---------------------------------------');
  console.log('Running Mode ------ ', Meteor.settings.public.env, ' ------');
  console.log('Running Version --- ', Meteor.settings.public.version, ' ----------');
  console.log('---------------------------------------');
});



// axios.defaults.baseURL = tokenUrl
// axios.defaults.headers.common['Authorization'] = 'Basic ' + base64code
// axios.post(tokenUrl, querystring.stringify({
//     'grant_type': 'client_credentials'
//   }))
//   .then(function (response) {
//     console.log(3333, response.data);
//   })
//   .catch(function (error) {
//     console.log(error, 4444);
//   })


